#pragma once

/*

JsonRPC Client

*/

#include <ArduinoJson.h>

#include "Version.h"

#include "Config.h"
#include "Debug.h"

#include "Base.hpp"

namespace ARDUINOJSONRPC_NAMESPACE {

enum class ResponseStatus
{
  Ok,
  Error,
  Ignored,
};

typedef bool (*RequestCallback_t)(const JsonVariantConst response,
                                  void* extra);

// macro to simplify definition of client request callback functions
#define JSONRPCCLIENTCALLBACK(funcName, responseArgName, extraArgName)        \
  bool funcName(const JsonVariantConst responseArgName,                       \
                void* extraArgName = nullptr)

class Client : public Base
{
public:
  Client()
    : _lastId(0)
    , _insideRequestCallback(false)
    , _registeredRequestId(0)
    , _registeredRequestCallback(nullptr)
    , _registeredRequestCallbackExtra(nullptr){};
  void createNotification(JsonVariant request,
                          const char* method = nullptr) const;
  void createRequest(JsonVariant request, const char* method = nullptr);

  void registerRequest(RequestCallback_t callback,
                       const jsonRpcId_t id = 0,
                       void* extra = nullptr);
  void unregisterRequest(void);
  bool requestCallbackPending(void);
  bool insideRequestCallback(void);

  ResponseStatus processResponse(const JsonVariantConst response,
                                 void* extra = nullptr);

  jsonRpcId_t getLastId(void) const;
  void setLastId(jsonRpcId_t id);
  jsonRpcId_t getNextId(void);

private:
  jsonRpcId_t _lastId;
  bool _insideRequestCallback;
  jsonRpcId_t _registeredRequestId;
  RequestCallback_t _registeredRequestCallback;
  void* _registeredRequestCallbackExtra;
};

jsonRpcId_t
Client::getLastId(void) const
{
  return this->_lastId;
}

void
Client::setLastId(jsonRpcId_t id)
{
  this->_lastId = id;
}

jsonRpcId_t
Client::getNextId(void)
{
  return ++this->_lastId ? this->_lastId : ++this->_lastId;
}

void
Client::createNotification(JsonVariant request, const char* method) const
{
  this->addJsonRpc(request);
  if (method) {
    if (not request["method"].is<char*>())
      request["method"] = method;
  }
}

void
Client::createRequest(JsonVariant request, const char* method)
{
  this->addJsonRpc(request);
  if (method) {
    if (not request["method"].is<char*>())
      request["method"] = method;
  }
  if (not request["id"].is<jsonRpcId_t>())
    request["id"] = this->getNextId();
}

void
Client::registerRequest(RequestCallback_t callback,
                        const jsonRpcId_t id,
                        void* extra)
{
  if (this->requestCallbackPending() and not this->insideRequestCallback()) {
    AJRPCDSN("WARNING: Dropping pending request callback for id ",
             this->_registeredRequestId);
    this->unregisterRequest();
  }
  this->_registeredRequestId = id ? id : this->getLastId();
  this->_registeredRequestCallback = callback;
  this->_registeredRequestCallbackExtra = extra;
  AJRPCDSNSNSN(
    "Registered callback at address ",
    reinterpret_cast<uintptr_t>(this->_registeredRequestCallback),
    " for id ",
    this->_registeredRequestId,
    " with extra data ",
    reinterpret_cast<uintptr_t>(this->_registeredRequestCallbackExtra));
}

bool
Client::requestCallbackPending(void)
{
  return this->_registeredRequestId and this->_registeredRequestCallback;
}

bool
Client::insideRequestCallback(void)
{
  return this->_insideRequestCallback;
}

void
Client::unregisterRequest(void)
{
  if (this->requestCallbackPending()) {
    AJRPCDSNSNSN(
      "Unregistering callback at address ",
      reinterpret_cast<uintptr_t>(this->_registeredRequestCallback),
      " for id ",
      this->_registeredRequestId,
      " with extra data ",
      reinterpret_cast<uintptr_t>(this->_registeredRequestCallbackExtra));
  }
  this->_registeredRequestId = 0;
  this->_registeredRequestCallback = nullptr;
  this->_registeredRequestCallbackExtra = nullptr;
}

ResponseStatus
Client::processResponse(const JsonVariantConst response, void* extra)
{
  if (not this->_registeredRequestCallback) {
    AJRPCDS("No Request callback registered. Ignoring this response.");
    return ResponseStatus::Ignored;
  }
  if (response["id"] == this->_registeredRequestId) {
    this->_insideRequestCallback = true;
    const bool success = this->_registeredRequestCallback(
      response, extra ? extra : this->_registeredRequestCallbackExtra);
    this->_insideRequestCallback = false;
    this->unregisterRequest();
    return success ? ResponseStatus::Ok : ResponseStatus::Error;
  } else {
    AJRPCDSNSNS("Request id ",
                response["id"].as<jsonRpcId_t>(),
                " does not match our registered id ",
                this->_registeredRequestId,
                ". Ignoring this response.");
    return ResponseStatus::Ignored;
  }
}

} // namespace ARDUINOJSONRPC_NAMESPACE
