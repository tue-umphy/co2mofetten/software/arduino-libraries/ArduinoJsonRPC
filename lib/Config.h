#pragma once

#define ARDUINOJSONRPC_NAMESPACE JsonRPC

// ARDUINOJSONRPC_USE_JSONRPC_MEMBER
//
// Whether the "jsonrpc":"2.0" pair should be included/asserted for responses
// and requests across the library. Default is 1 which means yes.
#ifndef ARDUINOJSONRPC_USE_JSONRPC_MEMBER
#define ARDUINOJSONRPC_USE_JSONRPC_MEMBER 1
#endif // #ifndef ARDUINOJSONRPC_USE_JSONRPC_MEMBER
