#pragma once
/*

JsonRPC Server

*/

#include <ArduinoJson.h>

#include "Version.h"

#include "Config.h"
#include "Debug.h"

#include "Base.hpp"

namespace ARDUINOJSONRPC_NAMESPACE {

enum class CallbackStatus
{
  Ok,
  InvalidParams,
  Error,
};

enum class MethodCallStatus
{
  Ok,
  MethodNotFound,
  InvalidParams,
  Error,
};

enum class RequestStatus
{
  RequestProcessed,
  InvalidRequest,
  NotificationProcessed,
  Unknown,
  Error,
};

// type for server method callback functions
typedef CallbackStatus (*ServerMethod_t)(JsonVariant response,
                                         JsonVariantConst params,
                                         JsonVariantConst id,
                                         void* extra);

// macro to simplify definition of server method callback functions
#define JSONRPCSERVERMETHOD(                                                  \
  funcName, idArgName, paramsArgName, responseArgName, extraArgName)          \
  ARDUINOJSONRPC_NAMESPACE::CallbackStatus funcName(                          \
    JsonVariant responseArgName,                                              \
    JsonVariantConst paramsArgName =                                          \
      ARDUINOJSONRPC_NAMESPACE::_dummyJsonVariantConst,                       \
    JsonVariantConst idArgName =                                              \
      ARDUINOJSONRPC_NAMESPACE::_dummyJsonVariantConst,                       \
    void* extraArgName = nullptr)

template<size_t N_METHODS>
class Server : public Base
{
public:
  Server(void)
    : _nMethods(0){};
  // WARNING: the `name` pointer must never be dangling
  bool registerMethod(const char* name, ServerMethod_t callback);
  void clearMethods(void);
  size_t getNMethods(void) const;
  MethodCallStatus callMethod(const char* name,
                              JsonVariant response = _dummyJsonVariantConst,
                              JsonVariantConst params = _dummyJsonVariantConst,
                              JsonVariantConst id = _dummyJsonVariantConst,
                              void* extra = nullptr) const;
  RequestStatus processRequest(JsonVariantConst request,
                               JsonVariant response,
                               void* extra = nullptr) const;
  void createResponse(JsonVariant response, jsonRpcId_t id) const;
  JsonObject createErrorSkeleton(JsonVariant response, jsonRpcId_t id) const;
  void createError(JsonVariant response,
                   ErrorCode code = ErrorCode::Unknown,
                   jsonRpcId_t id = 0,
                   const char* message = nullptr) const;
  void createError(JsonVariant response,
                   jsonRpcErrorCode_t code,
                   jsonRpcId_t id = 0,
                   const char* message = nullptr) const;

private:
  const char* _methodNames[N_METHODS];
  ServerMethod_t _methodCallbacks[N_METHODS];
  size_t _nMethods;
};

template<size_t N_METHODS>
void
Server<N_METHODS>::clearMethods(void)
{
  this->_nMethods = 0;
}

template<size_t N_METHODS>
size_t
Server<N_METHODS>::getNMethods(void) const
{
  return this->_nMethods;
}

template<size_t N_METHODS>
bool
Server<N_METHODS>::registerMethod(const char* name, ServerMethod_t callback)
{
  if (not name)
    return false;
  if (this->_nMethods >= N_METHODS)
    return false;
  this->_methodNames[this->_nMethods] = name;
  this->_methodCallbacks[this->_nMethods] = callback;
  this->_nMethods++;
  return true;
}

template<size_t N_METHODS>
MethodCallStatus
Server<N_METHODS>::callMethod(const char* name,
                              JsonVariant response,
                              JsonVariantConst params,
                              JsonVariantConst id,
                              void* extra) const
{
  if (not name)
    return MethodCallStatus::MethodNotFound;
  for (size_t i = 0; i < this->getNMethods(); i++) {
    if (strcmp(name, this->_methodNames[i]) == 0) {
      CallbackStatus status =
        this->_methodCallbacks[i](response, params, id, extra);
      switch (status) {
        case CallbackStatus::Ok:
          return MethodCallStatus::Ok;
        case CallbackStatus::InvalidParams:
          return MethodCallStatus::InvalidParams;
        case CallbackStatus::Error:
          return MethodCallStatus::Error;
        default:
          return MethodCallStatus::Error;
      }
    }
  }
  AJRPCDSN("No such method: ", name);
  return MethodCallStatus::MethodNotFound;
}

template<size_t N_METHODS>
RequestStatus
Server<N_METHODS>::processRequest(JsonVariantConst request,
                                  JsonVariant response,
                                  void* extra) const
{
  AJRPCDBG(AJRPCDF("Got input "); serializeJson(request, AJRPCDEBUGPRINT));
  MessageType type = this->inspect(request);
  if (not(type == MessageType::Request or type == MessageType::Notification)) {
    this->createError(response, ErrorCode::InvalidRequest, request["id"]);
    return RequestStatus::InvalidRequest;
  }
  AJRPCDBG(AJRPCDF("Got ");
           AJRPCDP(type == MessageType::Notification ? F("notification")
                                                     : F("request"));
           AJRPCDF(" for method '");
           AJRPCDP(request["method"].as<char*>());
           AJRPCDF("' with id ");
           serializeJson(request["id"], AJRPCDEBUGPRINT);
           AJRPCDF(" and params ");
           serializeJson(request["params"], AJRPCDEBUGPRINT););
  this->createResponse(response, request["id"]);
  const MethodCallStatus status = this->callMethod(
    request["method"], response, request["params"], request["id"], extra);
  if (type == MessageType::Notification)
    return RequestStatus::NotificationProcessed;
  AJRPCDBG(AJRPCDF("Response after calling '");
           AJRPCDP(request["method"].as<char*>());
           AJRPCDF("' method callback with id ");
           serializeJson(request["id"], AJRPCDEBUGPRINT);
           AJRPCDF(" and params ");
           serializeJson(request["params"], AJRPCDEBUGPRINT);
           AJRPCDF(" is ");
           serializeJson(response, AJRPCDEBUGPRINT);
           AJRPCDF(", status is ");
           AJRPCDP(static_cast<uint8_t>(status)););
  if (status == MethodCallStatus::Ok) // method was successful
    return RequestStatus::RequestProcessed;
  else { // method returned an error
    switch (status) {
      case MethodCallStatus::MethodNotFound:
        this->createError(response, ErrorCode::MethodNotFound, request["id"]);
        break;
      case MethodCallStatus::InvalidParams:
        this->createError(response, ErrorCode::InvalidParams, request["id"]);
        break;
      default:
        this->createError(response);
        break;
    }
    return RequestStatus::Error;
  }
}

template<size_t N_METHODS>
void
Server<N_METHODS>::createResponse(JsonVariant response, jsonRpcId_t id) const
{
  this->addJsonRpc(response);
  response.remove("error");
  if (not response["id"].is<jsonRpcId_t>())
    response["id"] = id ? id : _dummyJsonVariantConst;
}

template<size_t N_METHODS>
JsonObject
Server<N_METHODS>::createErrorSkeleton(JsonVariant response,
                                       jsonRpcId_t id) const
{
  this->addJsonRpc(response);
  response.remove("result");
  if (not response["id"].is<jsonRpcId_t>())
    response["id"] = id ? id : _dummyJsonVariantConst;
  return response["error"].is<JsonObject>()
           ? response["error"]
           : response.createNestedObject("error");
}

template<size_t N_METHODS>
void
Server<N_METHODS>::createError(JsonVariant response,
                               ErrorCode code,
                               jsonRpcId_t id,
                               const char* message) const
{
  JsonObject error = this->createErrorSkeleton(response, id);
  if (not error["code"].is<jsonRpcErrorCode_t>())
    error["code"] = static_cast<jsonRpcErrorCode_t>(
      code == ErrorCode::Unknown ? ErrorCode::InternalError : code);
  if (not error["message"].is<char*>()) {
    if (message)
      error["message"] = message;
    else {
      switch (code) {
        case ErrorCode::ParseError:
          error["message"] = F("Parse error");
          break;
        case ErrorCode::InvalidParams:
          error["message"] = F("Invalid params");
          break;
        case ErrorCode::MethodNotFound:
          error["message"] = F("Method not found");
          break;
        case ErrorCode::InvalidRequest:
          error["message"] = F("Invalid Request");
          break;
        case ErrorCode::InternalError:
          error["message"] = F("Internal error");
          break;
        default:
          error["message"] = F("unknown");
          break;
      }
    }
  }
}

template<size_t N_METHODS>
void
Server<N_METHODS>::createError(JsonVariant response,
                               jsonRpcErrorCode_t code,
                               jsonRpcId_t id,
                               const char* message) const
{
  JsonObject error = this->createErrorSkeleton(response, id);
  error["code"] = code;
  if (not error["message"].is<char*>()) {
    if (message)
      error["message"] = message;
  }
}

} // namespace ARDUINOJSONRPC_NAMESPACE
