#pragma once

/*

JsonRPC Base

*/

#include <ArduinoJson.h>

#include "Version.h"

#include "Config.h"
#include "Debug.h"

namespace ARDUINOJSONRPC_NAMESPACE {

JsonVariantConst _dummyJsonVariantConst;
JsonVariant _dummyJsonVariant;

enum class MessageType
{
  Request,
  Notification,
  ResponseSuccess,
  ResponseError,
  InvalidRequest,
  InvalidResponse,
  Unknown,
};

typedef uint16_t jsonRpcId_t;

typedef int16_t jsonRpcErrorCode_t;

enum class ErrorCode : jsonRpcErrorCode_t
{
  ParseError = -32700,
  InvalidRequest = -32600,
  MethodNotFound = -32601,
  InvalidParams = -32602,
  InternalError = -32603,
  Unknown = 0,
};

class Base
{
public:
  Base()
#if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
    : useJsonRpcMember(true)
#endif // #if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
        {};
  void addJsonRpc(JsonVariant request) const;

  MessageType inspect(JsonVariantConst message) const;

#if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
  bool useJsonRpcMember;
#endif // #if ARDUINOJSONRPC_USE_JSONRPC_MEMBER

private:
};

void
Base::addJsonRpc(JsonVariant request) const
{
#if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
  if (this->useJsonRpcMember)
    request["jsonrpc"] = "2.0";
#endif // #if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
}

MessageType
Base::inspect(JsonVariantConst message) const
{
  MessageType type = MessageType::Unknown;
  if (not message.is<JsonObject>())
    return MessageType::Unknown;
  const JsonObjectConst object = message.as<JsonObject>();
  const bool hasId = object.containsKey("id");
  const bool hasMethod = object.containsKey("method");
  const bool hasParams = object.containsKey("params");
  const bool hasError = object.containsKey("error");
  const bool hasResult = object.containsKey("result");
  if (
    // "method" must not coincide with "result" or "error"
    (hasMethod and (hasResult or hasError)) or
    // "result" and "error" must not coincide
    (hasResult and hasError) or
    // "params" must not concide with "result" or "error"
    (hasParams and (hasResult or hasError)))
    return MessageType::Unknown;
  if (hasMethod) { // looks like a Request/Notification
    if (not object["method"].is<char*>())
      return MessageType::InvalidRequest;
    if (hasParams) {
      if (not(object["params"].is<JsonArray>() or
              object["params"].is<JsonObject>()))
        return MessageType::InvalidRequest;
    }
    if (hasId) {
      type = MessageType::Request;
    } else {
      type = MessageType::Notification;
    }
  } else { // does not look like Request/Notification --> Response/Error
    if (hasResult) {
      type = MessageType::ResponseSuccess;
    } else if (hasError) {
      const JsonObjectConst error = object["error"];
      if (error["code"].is<jsonRpcErrorCode_t>() and
          error["message"].is<char*>())
        type = MessageType::ResponseError;
      else
        return MessageType::InvalidResponse;
      if (not hasId) // "id" must be given on error
        return MessageType::InvalidResponse;
    } else {
      return MessageType::Unknown;
    }
  }
#if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
  if (this->useJsonRpcMember) {
    if (not(object["jsonrpc"] == "2.0")) {
      switch (type) {
        case MessageType::Request:
          return MessageType::InvalidRequest;
        case MessageType::Notification:
          return MessageType::InvalidRequest;
        case MessageType::ResponseSuccess:
          return MessageType::InvalidResponse;
        case MessageType::ResponseError:
          return MessageType::InvalidResponse;
        default:
          return type;
      }
    }
  }
#endif // #if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
  return type;
}

} // namespace ARDUINOJSONRPC_NAMESPACE
