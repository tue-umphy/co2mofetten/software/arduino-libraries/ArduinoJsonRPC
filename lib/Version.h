#pragma once

#include <ArduinoJson.h>

#if ARDUINOJSON_VERSION_MAJOR < 6
#error "ArduinoJsonRPC requires at least ArduinoJson version 6"
#endif // #if ARDUINOJSON_VERSION_MAJOR < 6

#define AJRPC_HEX_VERSION(major, minor, patch)                                \
  (major * 65536 + minor * 256 + patch)
#define AJRPC_HEX_VERSION_MAJOR(version) (version >> 16)
#define AJRPC_HEX_VERSION_MINOR(version) ((version >> 8) & 0xFF)
#define AJRPC_HEX_VERSION_PATCH(version) (version & 0xFF)

// Library version in HEX: 0xMMmmpp (MM=major, mm=minor, pp=patch)
// The version is defined in this format because:
// - the expression can be evaluated by the C Preprocessor
// - bumpversion can handle it
//
// If you need to perform version checks in the preprocessor, e.g. whether the
// library version is at least as high as 1.2.3, use the format
//
// #if ARDUINOJSONRPC_VERSION >= AJRPC_HEX_VERSION(1,2,3)
// ... code for ArduinoJsonRPC version >= 1.2.3
// #endif
//
#define ARDUINOJSONRPC_VERSION AJRPC_HEX_VERSION(0, 5, 0)

// Major, Minor and Patch version parts for use in runtime code
#define ARDUINOJSONRPC_VERSION_MAJOR                                          \
  AJRPC_HEX_VERSION_MAJOR(ARDUINOJSONRPC_VERSION);
#define ARDUINOJSONRPC_VERSION_MINOR                                          \
  AJRPC_HEX_VERSION_MINOR(ARDUINOJSONRPC_VERSION);
#define ARDUINOJSONRPC_VERSION_PATCH                                          \
  AJRPC_HEX_VERSION_PATCH(ARDUINOJSONRPC_VERSION);
