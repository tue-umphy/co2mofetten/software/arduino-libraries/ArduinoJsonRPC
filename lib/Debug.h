#pragma once

/*

  Debug macros

*/
#define AJRPCDEBUGPRINT Serial
#define AJRPCDF(s)                                                            \
  {                                                                           \
    AJRPCDEBUGPRINT.print(F(s));                                              \
  };
#define AJRPCDP(s)                                                            \
  {                                                                           \
    AJRPCDEBUGPRINT.print(s);                                                 \
  };
#define AJRPCDH(s)                                                            \
  {                                                                           \
    AJRPCDF("0x");                                                            \
    AJRPCDEBUGPRINT.print(s, HEX);                                            \
  };
#define AJRPCDh(s)                                                            \
  {                                                                           \
    AJRPCDEBUGPRINT.print(s, HEX);                                            \
  };
#define AJRPCDL()                                                             \
  {                                                                           \
    AJRPCDEBUGPRINT.println();                                                \
  };

#ifndef ARDUINOJSONRPC_DEBUG
#define ARDUINOJSONRPC_DEBUG 0
#endif // #ifndef ARDUINOJSONRPC_DEBUG
#if ARDUINOJSONRPC_DEBUG
#define AJRPCDBG(cmd)                                                         \
  {                                                                           \
    AJRPCDF("ArduinoJsonRPC: ");                                              \
    cmd;                                                                      \
    AJRPCDL();                                                                \
    AJRPCDEBUGPRINT.flush();                                                  \
  };
#else
#define AJRPCDBG(cmd)
#endif

#define AJRPCDS(s) AJRPCDBG(AJRPCDF(s))
#define AJRPCDN(s) AJRPCDBG(AJRPCDP(s))
#define AJRPCDSN(s, n) AJRPCDBG(AJRPCDF(s); AJRPCDP(n))
#define AJRPCDSNS(s1, n, s2) AJRPCDBG(AJRPCDF(s1); AJRPCDP(n); AJRPCDF(s2))
#define AJRPCDSNSN(s1, n1, s2, n2)                                            \
  AJRPCDBG(AJRPCDF(s1); AJRPCDP(n1); AJRPCDF(s2); AJRPCDP(n2))
#define AJRPCDSNSNS(s1, n1, s2, n2, s3)                                       \
  AJRPCDBG(AJRPCDF(s1); AJRPCDP(n1); AJRPCDF(s2); AJRPCDP(n2); AJRPCDF(s3))
#define AJRPCDSNSNSH(s1, n1, s2, n2, s3, n3)                                  \
  AJRPCDBG(AJRPCDF(s1); AJRPCDP(n1); AJRPCDF(s2); AJRPCDP(n2); AJRPCDF(s3);   \
           AJRPCDH(n3))
#define AJRPCDSH(s, n) AJRPCDBG(AJRPCDF(s); AJRPCDH(n))
#define AJRPCDSHH(s, n1, n2) AJRPCDBG(AJRPCDF(s); AJRPCDH(n1); AJRPCDH(n2))
#define AJRPCDSHS(s1, n, s2) AJRPCDBG(AJRPCDF(s1); AJRPCDH(n); AJRPCDF(s2))
#define AJRPCDSHSH(s1, n1, s2, n2)                                            \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDH(n2))
#define AJRPCDSHSN(s1, n1, s2, n2)                                            \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDP(n2))
#define AJRPCDSNSH(s1, n1, s2, n2)                                            \
  AJRPCDBG(AJRPCDF(s1); AJRPCDP(n1); AJRPCDF(s2); AJRPCDH(n2))
#define AJRPCDSHSHS(s1, n1, s2, n2, s3)                                       \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDH(n2); AJRPCDF(s3))
#define AJRPCDSHSHSH(s1, n1, s2, n2, s3, n3)                                  \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDH(n2); AJRPCDF(s3);   \
           AJRPCDH(n3))
#define AJRPCDSHSNS(s1, n1, s2, n2, s3)                                       \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDP(n2); AJRPCDF(s3))
#define AJRPCDSNSHS(s1, n1, s2, n2, s3)                                       \
  AJRPCDBG(AJRPCDF(s1); AJRPCDP(n1); AJRPCDF(s2); AJRPCDH(n2); AJRPCDF(s3))
#define AJRPCDSNSNSN(s1, n1, s2, n2, s3, n3)                                  \
  AJRPCDBG(AJRPCDF(s1); AJRPCDP(n1); AJRPCDF(s2); AJRPCDP(n2); AJRPCDF(s3);   \
           AJRPCDP(n3))
#define AJRPCDSHSNSN(s1, n1, s2, n2, s3, n3)                                  \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDP(n2); AJRPCDF(s3);   \
           AJRPCDP(n3))
#define AJRPCDSHSHSNS(s1, n1, s2, n2, s3, n3, s4)                             \
  AJRPCDBG(AJRPCDF(s1); AJRPCDH(n1); AJRPCDF(s2); AJRPCDH(n2); AJRPCDF(s3);   \
           AJRPCDP(n3);                                                       \
           AJRPCDF(s4))

#define AJRPCSTOP(cmd)                                                        \
  {                                                                           \
    cmd;                                                                      \
    while (true)                                                              \
      ;                                                                       \
  };
