#pragma once

/*

JsonRPC Participant

*/

#include <ArduinoJson.h>

#include "Version.h"

#include "Config.h"
#include "Debug.h"

#include "Client.hpp"
#include "Server.hpp"

namespace ARDUINOJSONRPC_NAMESPACE {

enum class ProcessStatus
{
  RequestProcessed,
  NotificationProcessed,
  RequestError,
  RequestUnknown,
  InvalidRequest,
  ResponseProcessed,
  ResponseError,
  ResponseIgnored,
};

template<size_t N_METHODS>
class Participant
  : public Server<N_METHODS>
  , public Client
{
public:
  // explicitly use Client's version to overcome ambiguities
  void addJsonRpc(JsonVariant request) const { Client::addJsonRpc(request); };
  MessageType inspect(JsonVariantConst message) const
  {
    return Client::inspect(message);
  };

  ProcessStatus process(const JsonVariantConst input,
                        JsonVariant output = _dummyJsonVariant,
                        void* extra = nullptr);

private:
};

template<size_t N_METHODS>
ProcessStatus
Participant<N_METHODS>::process(const JsonVariantConst input,
                                JsonVariant output,
                                void* extra)
{
  const MessageType type = Client::inspect(input);
  if (type == MessageType::ResponseSuccess or
      type == MessageType::ResponseError) {
    ResponseStatus status = this->processResponse(input, extra);
    switch (status) {
      case ResponseStatus::Ok:
        return ProcessStatus::ResponseProcessed;
      case ResponseStatus::Error:
        return ProcessStatus::ResponseError;
      case ResponseStatus::Ignored:
        return ProcessStatus::ResponseIgnored;
      default:
        return ProcessStatus::ResponseIgnored;
    }
  } else {
    RequestStatus status = this->processRequest(input, output, extra);
    switch (status) {
      case RequestStatus::RequestProcessed:
        return ProcessStatus::RequestProcessed;
      case RequestStatus::NotificationProcessed:
        return ProcessStatus::NotificationProcessed;
      case RequestStatus::InvalidRequest:
        return ProcessStatus::InvalidRequest;
      case RequestStatus::Error:
        return ProcessStatus::RequestError;
      case RequestStatus::Unknown:
        return ProcessStatus::RequestUnknown;
      default:
        return ProcessStatus::RequestUnknown;
    }
  }
}
} // namespace ARDUINOJSONRPC_NAMESPACE
