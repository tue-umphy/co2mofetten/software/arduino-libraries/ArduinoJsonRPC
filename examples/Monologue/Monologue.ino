#include <ArduinoJsonRPC.hpp>

#if ARDUINOJSONRPC_VERSION < AJRPC_HEX_VERSION(0, 5, 0)
#error "This sketch requires at least ArduinoJsonRPC version 0.5.0"
#endif

using namespace JsonRPC;

// instantiate a participant instance that can register 2 server method
Participant<2> participant;

char name[20] = "Bob";

JSONRPCSERVERMETHOD(getNameMethod, id, params, response, extra)
{
  response["result"] = name;
  return CallbackStatus::Ok;
}

JSONRPCSERVERMETHOD(setNameMethod, id, params, response, extra)
{
  const char* newName = params[0].as<char*>();
  if (not newName)
    return CallbackStatus::InvalidParams;
  char s[100];
  sprintf(s, "name changed from %s to %s", name, newName);
  response["result"] = s;
  Serial.print(F("'setName' method callback: Changing the name from "));
  Serial.print(name);
  Serial.print(F(" to "));
  Serial.println(newName);
  strlcpy(name, newName, 20);
  return CallbackStatus::Ok;
}

JSONRPCCLIENTCALLBACK(getNameCallback, response, extra)
{
  // interpret the "extra" argument as a JsonVariant
  JsonVariant& request = *static_cast<JsonVariant*>(extra);
  const char* currentName = response["result"] | "";
  Serial.print(F("Client callback: response says name is "));
  Serial.println(currentName);
  // append a "by" to the current name (we want to rename Bob to Bobby)
  char newName[10];
  sprintf(newName, "%s%s", currentName, "by");
  // create a new request to set the new name
  participant.createRequest(request, "setName");
  JsonArray params = request.createNestedArray("params");
  params.add(newName);
  return true;
}

void
setup(void)
{
  randomSeed(analogRead(A0)); // make random() really random
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  Serial.println(F("ArduinoJsonRPC Participant Monologue example"));
  Serial.println();
  Serial.println(F("This example demonstrates the client and server "));
  Serial.println(F("capabilities of a Participant by letting it talk "));
  Serial.println(F("to itself."));
  Serial.println();
  // register the server methods
  participant.registerMethod("getName", getNameMethod);
  participant.registerMethod("setName", setNameMethod);
}

void
loop(void)
{
  StaticJsonDocument<300> requestDoc;
  StaticJsonDocument<300> responseDoc;
  JsonVariant request;
  JsonVariant response;
  // Client functionality: formulate a "getName" request
  request = requestDoc.to<JsonVariant>(); // clear the request
  participant.createRequest(request, "getName");
  participant.registerRequest(getNameCallback);
  Serial.print(F("Request: "));
  serializeJson(request, Serial);
  Serial.println();
  // Server functionality: inspect an input, detect that it's a request and
  // process it by calling the right callback
  response = responseDoc.to<JsonVariant>(); // clear the response
  participant.process(request, response);
  Serial.print(F("Response: "));
  serializeJson(response, Serial);
  Serial.println();
  // Client functionality: inspect an input, detect that it's a response and
  // process it by calling the callback if the id matches. If it matched,
  // forget the registered callback. If we reprocessed the same response, the
  // callback would not be invoked again. Also, pass an extra argument (here:
  // a pointer to an empty request) to the callback so that it can be filled
  // there.
  request = requestDoc.to<JsonVariant>(); // clear the request
  participant.process(response, _dummyJsonVariant, &request);
  Serial.print(F("Request created in Callback: "));
  serializeJson(request, Serial);
  Serial.println();
  // now process the request created by the callback
  response = responseDoc.to<JsonVariant>(); // clear the response
  participant.process(request, response);
  Serial.print(F("Response: "));
  serializeJson(response, Serial);
  Serial.println();
  while (true)
    ;
}
