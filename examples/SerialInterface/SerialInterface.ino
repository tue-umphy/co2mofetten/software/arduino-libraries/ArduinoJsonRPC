#include <ArduinoJsonRPC.hpp>

#if ARDUINOJSONRPC_VERSION < AJRPC_HEX_VERSION(0, 5, 0)
#error "This sketch requires at least ArduinoJsonRPC version 0.5.0"
#endif

using namespace JsonRPC;

// instantiate a server instance that can register 1 method
Server<1> server;

JSONRPCSERVERMETHOD(sumMethod, id, params, response, extra)
{
  long sum = 0;
  const size_t nParams = params.size(); // this operation is ”slow”
  if (not params.is<JsonArray>() or not nParams) {
    JsonObject error = response.createNestedObject("error");
    error["data"] = "array of numbers is required";
    return CallbackStatus::InvalidParams;
  }
  // TODO: The book ”Mastering ArduinoJson 6” explicitly states that the
  // following way of iterating through a JsonArray is bad (slow) due to the
  // JsonArray being stored as liked list. Instead it proposes to use C++11's
  // ”range-based for loop”-feature. Unfortunately, I couldn't get it to work
  // in this context.
  for (size_t i = 0; i < nParams; i++) {
    JsonVariantConst val = params[i]; // this operation is ”slow”
    if (val.is<long>())
      sum += val.as<long>();
    else {
      JsonObject error = response.createNestedObject("error");
      response.remove("result");
      char msg[40] = "non-numeric value: ";
      serializeJson(val, msg + strlen(msg), 40 - strlen(msg));
      error["data"] = msg;
      return CallbackStatus::InvalidParams;
    }
  }
  response["result"] = sum;
  return CallbackStatus::Ok;
}

void
setup(void)
{
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  Serial.println(F("JsonRPCServer Serial Interface demonstration"));
  Serial.println();
  Serial.println(F("Write or copy-paste Json-RPC requests here, the server "));
  Serial.println(F("will parse and process it and return a response."));
  Serial.println();
  Serial.println(F("Hint: If you don't see what you type, enable "));
  Serial.println(F("'local echo' in your serial monitor."));
  Serial.println();
  Serial.setTimeout(1e3); // wait up to 1 second for the input
  // register the server methods
  server.registerMethod("sum", sumMethod);
  Serial.print(F(" Request: "));
}

void
loop(void)
{
  if (not Serial.available())
    return; // idle if there is nothing to read
  StaticJsonDocument<200> request;
  StaticJsonDocument<200> response;
  JsonVariant responseVariant = response.to<JsonVariant>();
  // parse the Json
  DeserializationError error = deserializeJson(request, Serial);
  Serial.println();
  delay(100); // give Serial a little time to recieve trailing bogus
  if (Serial.available()) {
    Serial.print(F("WARNING: dropping "));
    Serial.print(Serial.available());
    Serial.println(" trailing input bytes");
  }
  while (Serial.available()) {
    Serial.read();
    delay(1);
  }
  if (error) { // if parsing failed, create a Parse error
    Serial.print(F("Response: "));
    server.createError(responseVariant, ErrorCode::ParseError);
    serializeJson(responseVariant, Serial);
  } else { // otherwise, process the request
    MessageType type = server.inspect(request.as<JsonVariant>());
    Serial.print(F("    Type: "));
    switch (type) {
      case MessageType::Request:
        Serial.print(F("Request"));
        break;
      case MessageType::Notification:
        Serial.print(F("Notification"));
        break;
      case MessageType::ResponseSuccess:
        Serial.print(F("successful Response"));
        break;
      case MessageType::ResponseError:
        Serial.print(F("Error Response"));
        break;
      case MessageType::InvalidRequest:
        Serial.print(F("Invalid Request"));
        break;
      case MessageType::InvalidResponse:
        Serial.print(F("Invalid Response"));
        break;
      case MessageType::Unknown:
        Serial.print(F("Unknown"));
        break;
      default:
        Serial.print(F("???"));
        break;
    }
    Serial.println();
    RequestStatus status =
      // the template argument is the capacity of a temporary
      // StaticJsonDocument for the callback's result
      server.processRequest(request.as<JsonVariant>(), responseVariant);
    Serial.print(F("Response: "));
    if (status == RequestStatus::NotificationProcessed) { // notification
      Serial.print(F("(none, because the Request was a Notification)"));
    } else { // normal response
      serializeJson(responseVariant, Serial);
    }
  }
  Serial.println();
  Serial.print(F(" Request: "));
}
