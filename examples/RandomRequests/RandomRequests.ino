#include <ArduinoJsonRPC.hpp>

#if ARDUINOJSONRPC_VERSION < AJRPC_HEX_VERSION(0, 5, 0)
#error "This sketch requires at least ArduinoJsonRPC version 0.5.0"
#endif

// instantiate a server instance that can register 1 method
JsonRPC::Server<1> server;
JsonRPC::Client client;

// the JSONRPCSERVERMETHOD macro creates a signature for a callback function
// called `infoMethod` taking JsonVariant arguments called `id`, `params` and
// `result`
JSONRPCSERVERMETHOD(sumMethod, id, params, response, extra)
{
  using namespace JsonRPC;
  // interpret the 'extra' argument as Print* and dereference it
  // WARNING: the 'extra' argument MUST be castable to the Print type.
  // Make sure to pass an appropriate pointer to
  // JsonRPC::Server.processRequest(). Otherwise, the code will crash/behave
  // strangely.
  Print& extraPort = *static_cast<Print*>(extra);
  extraPort.println("The 'sum' method says Hi from the extra port!");
  long sum = 0;
  if (not params.is<JsonArray>()) {
    JsonObject error = response.createNestedObject("error");
    error["data"] = "array of numbers is required";
    return CallbackStatus::InvalidParams;
  }
  // TODO: The book ”Mastering ArduinoJson 6” explicitly states that the
  // following way of iterating through a JsonArray is bad (slow) due to the
  // JsonArray being stored as liked list. Instead it proposes to use C++11's
  // ”range-based for loop”-feature. Unfortunately, I couldn't get it to work
  // in this context.
  const size_t nParams = params.size(); // this operation is ”slow”
  for (size_t i = 0; i < nParams; i++) {
    JsonVariantConst val = params[i]; // this operation is ”slow”
    if (val.is<long>())
      sum += val.as<long>();
    else {
      JsonObject error = response.createNestedObject("error");
      response.remove("result");
      char msg[40] = "non-numeric value: ";
      serializeJson(val, msg + strlen(msg), 40 - strlen(msg));
      error["data"] = msg;
      return CallbackStatus::InvalidParams;
    }
  }
  response["result"].set(sum);
  return CallbackStatus::Ok;
}

void
setup(void)
{
  // initialize the serial port
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  Serial.println("JsonRPCServer demonstration");
  Serial.println("===========================");
  Serial.println();
  // make random() really random
  randomSeed(analogRead(A0));
  // tell the server to call the sumMethod function on method "sum"
  server.registerMethod("sum", &sumMethod);
  // NOTE: only the pointer to the method name is stored, so it MUST be
  // available for the whole lifetime of the JsonRPCServer, i.e. in the RAM's
  // globals section which is the case with string literals for example
#if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
  if (random(0, 10) > 5) {
    server.useJsonRpcMember = false;
    Serial.println("The server ignores the \"jsonrpc\" member in this run");
    Serial.println();
  }
#endif // #if ARDUINOJSONRPC_USE_JSONRPC_MEMBER
}

void
loop(void)
{
  // capacity calculated with the ArduinoJson Assistant:
  // https://arduinojson.org/v6/assistant/
  const size_t capacity = JSON_ARRAY_SIZE(5) + JSON_OBJECT_SIZE(4) + 100;
  StaticJsonDocument<capacity> requestDoc;
  JsonVariant request = requestDoc.to<JsonVariant>();
  // create a Json-RPC Request/Notification
  if (random(0, 10) > 2) // randomly
    client.createRequest(request, "sum");
  else
    client.createNotification(request, "sum");
  int32_t sum = 0;
  if (random(0, 10) > 1) {
    // set the “params“ to an array of random numbers
    JsonArray params = request.createNestedArray("params");
    for (size_t i = 0; i < random(2, 10); i++) {
      const int16_t val = random(-0xFFFF, 0xFFFF);
      params.add(val);
      sum += val;
    }
    // randomly add a non-numeric string to produce an error
    if (random(0, 10) > 5)
      params.add("bogus");
  } else
    request["params"] = "derp";

  // print the request
  Serial.print("Request: ");
  serializeJson(request, Serial);
  Serial.println();
  Serial.println();

  // We need to allocate a reponse JsonDocument where the server can write its
  // response into.
  StaticJsonDocument<100> response;
  JsonVariant responseVariant = response.to<JsonVariant>();
  JsonRPC::RequestStatus requestStatus =
    // the template argument is the capacity of a temporary StaticJsonDocument
    // for the callback's result
    server.processRequest(request.as<JsonVariant>(),
                          responseVariant,
                          // pass an extra argument to the method callback
                          &Serial);

  if (requestStatus == JsonRPC::RequestStatus::NotificationProcessed) {
    Serial.println("This request was a notification. No response to give!");
  } else {
    // print the response
    Serial.println();
    Serial.print("Response: ");
    serializeJson(response, Serial);
    Serial.println();
    Serial.println();

    // check the result for errors/correctness
    if (response.containsKey("error")) {
      Serial.print("An error occured: ");
      Serial.print(response["error"]["message"] | "");
      Serial.print(": ");
      Serial.print(response["error"]["data"] | "");
      Serial.println();
    } else {
      Serial.print("Sum is ");
      Serial.print(response["result"] == sum ? "correct" : "wrong");
      Serial.println("!");
    }
  }

  Serial.println();
  Serial.println("Run this sketch several times, it contains random()ness!");
  Serial.println();
  while (true)
    ; // do nothing forevermore
}
