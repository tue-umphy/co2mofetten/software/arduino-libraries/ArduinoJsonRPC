# ArduinoJsonRPC Arduino Library

Arduino RPC library based on [ArduinoJson](https://arduinojson.org).

## Documentation

For now, see the
[`ArduinoJsonRPC.hpp`](https://gitlab.com/tue-umphy/co2mofetten/arduino-libraries/ArduinoJsonRPC/blob/master/ArduinoJsonRPC.hpp)
file for a method description or have a look at the `examples` folder.



